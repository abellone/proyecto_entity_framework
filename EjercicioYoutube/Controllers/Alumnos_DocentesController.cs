﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EjercicioYoutube.Data;
using EjercicioYoutube.Models;


namespace EjercicioYoutube
{
    public class Alumnos_DocentesController : Controller
    {
        private readonly EjercicioYoutubeContext _context;
        public UnionListasViewModel modelNuevo = new UnionListasViewModel();

        public Alumnos_DocentesController(EjercicioYoutubeContext context)
        {
            _context = context;
        }

     // GET: Alumnos_Docentes
        public async Task<IActionResult> Index()
        {
            var query = _context.Alumnos_Docentes
                .Include(x => x.Alumno)
                .Include(x => x.Docente);

            var list = await query.ToListAsync();
            return View(list);
        }

        // GET: Alumnos_Docentes/Create
        public async Task<IActionResult> CreateAsync()
        {
            modelNuevo.Alumno = await _context.Alumnos.ToListAsync();
            modelNuevo.Docente = await _context.Docentes.ToListAsync();
            return View(modelNuevo);
        }

        // GET: Alumnos_Docentes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumnos_Docentes = await _context.Alumnos_Docentes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (alumnos_Docentes == null)
            {
                return NotFound();
            }

            return View(alumnos_Docentes);
        }

        // POST: Alumnos_Docentes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create()
        {
            Alumnos_Docentes alumnos_Docentes = new Alumnos_Docentes();

            var alumno = await _context.Alumnos.FindAsync(int.Parse(Request.Form["selectAlumno"]));
            var docente = await _context.Docentes.FindAsync(int.Parse(Request.Form["selectDocente"]));

            var query = _context.Alumnos_Docentes
                .Include(x => x.Alumno)
                .Include(x => x.Docente);

            var list = await query.ToListAsync();

            if (ModelState.IsValid)
            {
                alumnos_Docentes.Alumno = alumno;
                alumnos_Docentes.Docente = docente;
                if (alumno != null && docente != null)
                {

                    foreach (var relacion in list)
                    {
                        if (!list.Any(x => x.AlumnoId.Equals(alumno.Id) && x.DocenteId.Equals(docente.Id)))
                        {
                            _context.Add(alumnos_Docentes);
                            await _context.SaveChangesAsync();
                            return RedirectToAction(nameof(Index));
                        }
                    }
                    return RedirectToAction(nameof(Create));
                }
                else
                {
                    return RedirectToAction(nameof(Create));

                }
            }
            return View(alumnos_Docentes);
        }

        // GET: Alumnos_Docentes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumnos_Docentes = await _context.Alumnos_Docentes.FindAsync(id);
            if (alumnos_Docentes == null)
            {
                return NotFound();
            }
            return View(alumnos_Docentes);
        }

        // POST: Alumnos_Docentes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Id_alumno,Id_docente")] Alumnos_Docentes alumnos_Docentes)
        {
            if (id != alumnos_Docentes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(alumnos_Docentes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Alumnos_DocentesExists(alumnos_Docentes.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(alumnos_Docentes);
        }

        // GET: Alumnos_Docentes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumnos_Docentes = await _context.Alumnos_Docentes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (alumnos_Docentes == null)
            {
                return NotFound();
            }

            return View(alumnos_Docentes);
        }

        // POST: Alumnos_Docentes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var alumnos_Docentes = await _context.Alumnos_Docentes.FindAsync(id);
            _context.Alumnos_Docentes.Remove(alumnos_Docentes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool Alumnos_DocentesExists(int id)
        {
            return _context.Alumnos_Docentes.Any(e => e.Id == id);
        }
    }
}
