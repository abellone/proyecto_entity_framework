﻿using EjercicioYoutube.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EjercicioYoutube.Models
{
    public class UnionListasViewModel
    {
        public List<Docentes> Docente { get; set; }
        public List<Alumnos> Alumno { get; set; }
    }

}
