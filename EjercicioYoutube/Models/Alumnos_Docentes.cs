﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EjercicioYoutube.Models
{
    public class Alumnos_Docentes
    {
        public int Id { get; set; }

        [Column("id_alumno")]
        public int AlumnoId{ get; set; }

        [Column("id_docente")]
        public int DocenteId { get; set; }
        public virtual Alumnos Alumno { get; set; }
        public virtual Docentes Docente { get; set; }

    }


}
