﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EjercicioYoutube.Models
{
    public class Alumnos
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Edad { get; set; }
        public char Sexo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public virtual ICollection<Alumnos_Docentes> Alumnos_Docentes { get; set; }

    }
}
