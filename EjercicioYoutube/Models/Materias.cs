﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EjercicioYoutube.Models
{
    public class Materias
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime DiayHorario { get; set; }

        [Column("id_docente")]
        public int DocenteId { get; set; }
        public virtual Docentes Docente { get; set; }

    }
}
