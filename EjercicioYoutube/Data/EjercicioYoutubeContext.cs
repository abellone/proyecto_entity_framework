﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EjercicioYoutube.Models;

namespace EjercicioYoutube.Data
{
    public class EjercicioYoutubeContext : DbContext
    {
        public EjercicioYoutubeContext()
        {
        }


        //Lo que administra la base de datos, la informacion de configuracion, la conexion...
        public EjercicioYoutubeContext (DbContextOptions<EjercicioYoutubeContext> options)
            : base(options)
        {
        }

        //Lo que nos va a permitir realizar modificaciones en la base de datos:
        public DbSet<EjercicioYoutube.Models.Alumnos> Alumnos { get; set; }

        //Lo que nos va a permitir realizar modificaciones en la base de datos:
        public DbSet<EjercicioYoutube.Models.Docentes> Docentes { get; set; }

        //Lo que nos va a permitir realizar modificaciones en la base de datos:
        public DbSet<EjercicioYoutube.Models.Alumnos_Docentes> Alumnos_Docentes { get; set; }

        //Lo que nos va a permitir realizar modificaciones en la base de datos:
        public DbSet<EjercicioYoutube.Models.Materias> Materias { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //relacion N-N entre Alumno y Docente
            modelBuilder.Entity<Alumnos_Docentes>().ToTable("Alumnos_Docentes").HasKey(x => x.Id);

            modelBuilder.Entity<Alumnos_Docentes>().ToTable("Alumnos_Docentes")
                .HasOne(x => x.Alumno)
                .WithMany(x => x.Alumnos_Docentes);

            modelBuilder.Entity<Alumnos_Docentes>().ToTable("Alumnos_Docentes")
                .HasOne(x => x.Docente)
                .WithMany(x => x.Alumnos_Docentes);
        }

    }
}
